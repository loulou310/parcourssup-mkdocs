# Contributeurs

Voici la liste des personnes qui m'ont aidés a réaliser ce site en ouvrant un ticket ou une demande de fusion.

## Rédaction

Ces utilisateurs ont corrigés des fautes de syntaxe ou d'orthographe

[![](https://github.com/hugo-hamet.png?size=100)](https://framagit.org/hugo-hamet.png)