---
tags:
    - Python
    - Traitement de données
---

# Statistiques sur les invocations dans Genshin Impact.

## Introduction

Genshin Impact est un jeu [gacha](https://fr.wikipedia.org/wiki/Gacha_(genre_de_jeu_vid%C3%A9o)). Il est donc intéressant de faire des statistiques sur les invocations. Le but de ce projet, contrairement aux cours de NSI en rapport avec le traitement de données, est de gérer tout le processus, de la collecte des données, aux analyses statistiques

??? info "Lexique"

    1. x étoiles : Degré de rareté de l'objet obtenu, de 1 pour un objet commun à 5 pour un obter très rare
    2. 50/50 : Fait d'obtenir l'objet en tête d'invocation. On dit "perdre son 50/50" lorsqu'on obtient pas l'objet exclusif de l'invocation

Dans cette analyse, nous allons nous pencher sur les invocations évènements de personnages

## Fonctionnement du gacha

???+ quote "Extrait du règlement de l'invocation évènement de personnage"
    
    [Objets 5★]  
    Taux d'obtention de base d'un personnage 5★ : 0,600 %. Taux d'obtention total (obtention garantie incluse) : 1,600 %. L'obtention d'un personnage 5★ est garantie au moins une fois tous les 90 vœux.
    Lorsque vous tirez un personnage 5★ pour la première fois, il y a 50,000 % de chance qu'il s'agisse du personnage en tête de l'invocation. Si ce n'est pas le cas, le personnage sera donc garanti lors de votre prochain tirage d'un personnage 5★.  
    [Objets 4★]  
    Taux d'obtention de base d'un objet 4★ : 5,100 %. Taux d'obtention de base d'un personnage 4★ : 2,550 %. Taux d'obtention de base d'une arme 4★ : 2,550 %. Taux d'obtention total d'un   objet 4★ (obtention garantie incluse) : 13,000 %. L'obtention d'un objet 4★ ou plus est garantie au moins une fois tous les 10 vœux effectués. Le taux d'obtention d'un objet 4★ grâce  à la garantie d'obtention est de 99,400 % et le taux d'obtention d'un objet 5★ grâce à la garantie d'obtention est de 0,600 %.
    Lorsque vous tirez un objet 4★ pour la première fois, il y a 50,000 % de chance qu'il s'agisse d'un des personnages en tête d'invocation. Si ce n'est pas le cas, l'un de ces personnages sera donc garanti lors de votre prochain tirage d'un objet 4★. Si vous obtenez un  objet 4★ par le biais d'un vœu, la probabilité d'obtention de chaque personnage 4★ à la une est égale.

Les invoactions disposent également d'un système de "pitié" : La chance d'obtenir un personnage 5 étoiles augmente avec le nombre de voeux effectués depuis le dernier personnage.  

La "pitié forte" est la garantie d'obtention d'un personnage 5 étoiles : 90

Aucune information n'est cependant fournie pour la "pitié douce". D'après des calculs effectués avec une base de donnée fournie par le site internet [genshin-wishes.com](https://genshin-wishes.com), la "pitié douce" est atteinte après 75 voeux. La probabilité d'obtenir un personnage 5 étoiles augmente à partir de ce seuil, pour atteindre 100% après 90 voeux.

## Environnement de génération de données

Pour des raisons financières et pratiques évidentes, le système [Grasscutter](https://github.com/grassutter/Grasscutter) sera utilisé. Ce logiciel permet d'émuler le serveur du jeu, et de s'y connecter en utilisant un proxy. Utiliser ce logicel me permettra d'obtenir des tickets d'invocations gratuitement. Il faut tout de même noter une légère modification du système d'invocation : Il est possible d'obtenir n'importe quel objet de la poule de récompense permanente lors de l'invocation d'un objet 5 étoiles, arme ou personnage. En temps normal, il est seulement possible d'obtenir un objet du même type que l'invocation. Cependant, l'obtention d'un des objets de la poule de récompenses permanentes entraîne l'activation de la garantie d'obtention du personnage 5 étoiles en tête de l'invocation. Enfin, on admet que le reste du mécanisme d'invocation n'a pas été altéré.

[Poule des objets](https://genshin-impact.fandom.com/wiki/Wanderlust_Invocation#Item_Pool){.md-button}

## Déroullement des invocations

Les invocations seront réalisées sur un nouveau compte, créé pour l'occasion. Cela nous garantira l'absence de "pitié". Le but des invocations est d'obtenir une fois chaque personnage d'évènement disponible à la version actuelle : 2.8. Les invocations de "retour", c'est-à-dire les invocations d'un personnage 5 étoiles déjà présent lors d'un évènement d'invocation précédent, seront ignorées. Les personnages seront donc invoqués dans l'ordre de leur apparition. Les invocations seront faites une par une. Chaque objet obtenu est reporté dans le jeu de données.

## Formatage des données

Les données seront stockées dans un fichier csv, sous la forme suivante : 

```
Nom,Rareté,Type,ID évènement d'invocation,En tête ?
```

Explications des clés : 

- Nom : Nom de l'obet obtenu
- Rareté : Degré de rareté de l'obet obtenu en étoiles, de 1 à 5
- Type : Personnage ou arme
- ID évènement d'invocation : Id de l'évènement d'invocation de personnage, voir [ce fichier](https://framagit.org/loulou310/genshin-data/-/blob/main/Banner_ID.txt)
- En tête? : Si le personnage obtenu est en tête de l'évènement de l'invocation (Booléen). Non applicable aux objets 3 étoiles, car il n'y a pas d'objets 3 étoiles en tête d'invocation.

## Retour sur la collecte de données

Le rassemblement des données est terminé. Ce processus à duré le temps des vacances d'été 2022. Cela était long et assez répétitif. J'ai pris énormément de retard car je perdais un peu d'intérêt pour le jeu, et la répétitivité de la collecte m'enunyait, mais j'ai su perséver. Ce retard m'a obligé a faire une bonne partie de la collecte des données à distance, via le logiciel AnyDesk car la base de donnée n'est pas synchronisée, mais cela n'était pas optimal dû à la rapidité de la connection internet chez moi. Grasscutter utilise une base de donnée mongoDB, et j'ai déja utilisé le cluster d'essai de MongoDBAtlas, donc il est possible d'utiliser ce cluster afin de pouvoir faire la collecte à distance.

Le jeu de données produit ne fait pas moins de 1442 lignes, en-têtes inclus.

J'ai également inclus dans le repo l'export de la collection correspondant au gacha, ainsi que le manuel généré par Grasscutter afin de savoir à quoi correspondent les IDs dans la collection.

## Status

- Collecte des données
- → Analyse
- Résultats
