---
tags:
    - Node.JS
    - JavaScript
    - Application
---
# GDrive CLI

Ce projet est né d'un besoin : celui de pouvoir synchroniser des fichiers stockés dans le cloud depuis un terminal. J'ai tout de suite pensé a git, et même Framagit, mais ce système n'est vraiment pas adapté pour le stockage cloud, notamment de la musique. Après de petites recherches, je n'ai pas trouvé d'outils répondant à mes besoins pour Google Drive. J'ai également trouvé MEGAcmd, mais je préférais utiliser Google Drive car je me trouve souvent en limite de transfert avec Mega. J'ai donc opté pour créer mon propre outil. Le projet est disponible [ici](https://framagit.org/loulou310/gdrive-shell).

Voici cependant quelques extraits de code : 

=== "Récupération d'un token enregistré"

    Cette section sert a vérifier si un token d'authentification à l'API de Google Drive est présent dans le répertoire courrant.
    
    ```js
    async function loadSavedCredentialsIfExists() {
        try {
            const content = await fs.readFile(TOKEN_PATH);
            const credentials = JSON.parse(content);
            return google.auth.fromJSON(credentials)
        } catch (err) {
            return null;
        }
    }
    ```

=== "Enregistrement d'un token"
    
    Cette fonction enregistre un token récupéré

    ```js
    async function saveCredentials(client) {
        const content = await fs.readFile(CREDENTIALS_PATH);
        const keys = JSON.parse(content)
        const key = keys.installed || keys.web
        const payload = JSON.stringify({
            type: 'authorized_user',
            client_id: key.client_id,
            client_secret: key.client_secret,
            refresh_token: client.credentials.refresh_token

        });
        await fs.writeFile(TOKEN_PATH, payload);
    }
    ```

=== "Authorization auprès des serveurs de Google"

    Cette fonction permet de s'authentifier auprès des serveur de Google et de récupérer un token

    ```js
    async function authorize() {
        let client = await loadSavedCredentialsIfExists();
        if (client) {
            return client;
        }
        client = await authenticate({
            scopes: SCOPES,
            keyfilePath: CREDENTIALS_PATH,
        });
        if (client.credentials) {
            await saveCredentials(client);
        }
        return client
    }
    ```

=== "Lister des fichiers"

    Cette fonction permet de récupérer jusqu'a 10 fichiers

    ```js
    async function listFiles(authClient) {
        const drive = google.drive({version: "v3", auth: authClient});
        const res = await drive.files.list({
            pageSize: 10,
            fields: "nextPageToken, files(id, name)",
        })
        const files = res.data.files
        if (files.length == 0) {
            console.log("Aucun fichiers trouvés")
            return
        }

        console.log("Fichiers : ")
        files.map((files) => {
            console.log(`${files.name} (${files.id})`)
        })
    }
    ```


Cette solution fonctionne mais nécessite de s'authentifier à travers un navigateur internet à travers le système OAuth2. Je pense quand même utiliser MEGAcmd car il supporte une authentification via le terminal et permet de synchroniser des fichiers de cette façon.
